package br.com.itau.Cartao.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Cartao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @NotNull(message = "Número do cartao não pode ser nulo")
    @NotBlank(message = "Número do cartão não pode ser vazio")
    private String numero;


    @NotNull(message = "úmero não pode ser nulo")
    private Long clienteId;

    @NotNull(message = "Ativo não  pode ser nulo")
    private Boolean ativo;

    public Long getClienteId() {

        return clienteId;
    }

    public void setClienteId(Long clienteId) {

        this.clienteId = clienteId;
    }

    public String getNumero() {

        return numero;
    }

    public void setNumero(String numero) {

        this.numero = numero;
    }

    public Boolean getAtivo() {

        return ativo;
    }

    public void setAtivo(Boolean ativo) {

        this.ativo = ativo;
    }

    public Integer getId() {
    }
}
