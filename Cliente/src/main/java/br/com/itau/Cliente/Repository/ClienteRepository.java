package br.com.itau.MicroServicoCliente.Repository;

import br.com.itau.MicroServicoCliente.Models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente,Integer > {

}




