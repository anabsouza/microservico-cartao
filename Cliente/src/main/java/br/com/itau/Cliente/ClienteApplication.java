package br.com.itau.MicroServicoCliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableFeignClients
public class MicroServicoClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServicoClienteApplication.class, args);
	}

}
