package br.com.itau.MicroServicoCliente.Service;

import br.com.itau.MicroServicoCliente.Models.Cliente;
import br.com.itau.MicroServicoCliente.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ClienteService
{
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criar (Cliente cliente)
    {
        return  clienteRepository.save(cliente);
    }

    public Cliente buscarPorId(int id)
    {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent())
        {
            Cliente cliente = clienteOptional.get();
            return  cliente;
        }
        else
        {
            throw new RuntimeException("O cliente não foi encontrado");
        }
    }
}
