package br.com.itau.MicroServicoCliente.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message =  "Número não pode ser nulo")
    @NotBlank(message = "Número não pode ser vazio")
    private String numero;

    @NotNull(message =  "Cliente não pode ser nulo")
    private ClienteId clienteId;

    @NotNull(message =  "Ativo não  pode ser nulo")
    private Boolean ativo;


    public String getId() {
        return id;
    }

    public void setId(Integer Id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Booelean ativo) {
        this.ativo = ativo;
    }

}



